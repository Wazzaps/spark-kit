# from Packages.Base import Interfaces_linux, Devices_all
#
#
# def get_device_identifier(dev):
#     return dev.identifier
#
#
# def cmp_devices(a, b):
#     identified_a = {interface: list(map(get_device_identifier, a[interface])) for interface in Interfaces_linux.modules}
#     identified_b = {interface: list(map(get_device_identifier, b[interface])) for interface in Interfaces_linux.modules}
#     return identified_a == identified_b
#
#
# def seek():
#     new_devices = {interface: [] for interface in Interfaces_linux.modules}
#     for interface_name, interface in Interfaces_linux.modules.items():
#         for device in interface.seek():
#             try:
#                 dev = Devices_all.mapping_table[device[:2]]
#                 new_devices[interface_name].append(dev[1].Device(device, dev[1]))
#             except KeyError:
#                 raise EnvironmentError('Missing libraries for {}/{}'.format(device[0], device[1]))
#     if cmp_devices(Interfaces_linux.devices, new_devices):
#         return Interfaces_linux.devices
#     else:
#         Interfaces_linux.devices = new_devices
#         return new_devices
#
#
# Interfaces_linux.init()
# seek()

# import threading
# import time
#
# import Controllers.PackageLoader
# from Controllers.DeviceManager import DeviceManager
#
# Controllers.PackageLoader.init()
# interfaces = Controllers.PackageLoader.modules['Interfaces']
# lock = threading.Lock()
# dm = DeviceManager(interfaces, lock)
#
# while True:
#     print(dm.devs_by_iface)
#     time.sleep(3)

# from skit_lib.Controllers.CSVCompressor import compress
# import gzip
#
# with open('/home/david/Documents/sk_comp/sparkkit_out.csv', 'r') as inp,\
#      gzip.open('/home/david/Documents/sk_comp/sparkkit_out_flt.dat.gz', 'wb') as out:
#     compress(inp, out)

import time
import bluetooth
import socket
import subprocess

# sock = socket.socket(socket.AF_BLUETOOTH, socket.BTPROTO_RFCOMM)

# print(dir(bluetooth))
# nearby_devices = bluetooth.discover_devices(duration=8, lookup_names=True,
#                                             flush_cache=True, lookup_class=False)
# print(nearby_devices)
# devs = subprocess.check_output(['hcitool', '-i', 'hci1', 'scan']).split('\n')[1:]
# print(devs)

# sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
# try:
#     sock.connect(('00:21:13:04:73:63', 1))
#     print(sock)
#     sock.settimeout(3)
#     sock.send(bytes(0))
#     sock.send(bytes(0))
#     for _ in range(100):
#         print(sock.recv(1))
#     time.sleep(5)
#     sock.close()
# except Exception as e:
#     print(e)

serverMACAddress = '00:21:13:04:73:63'
port = 1
s = socket.socket(socket.AF_BLUETOOTH, socket.SOCK_STREAM, socket.BTPROTO_RFCOMM)
s.connect((serverMACAddress, port))
s.settimeout(None)
print('Connected')
for _ in range(100):
    try:
        print(s.recv(1))
    except socket.timeout:
        break
print('Flushed')
s.send(b'\0')
s.settimeout(None)
for _ in range(100):
    print(s.recv(1))
s.close()
