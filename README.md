Spark Kit
=========

This tool is made to help students engage with science and scientific sensors specifically without
the burden of cost or technical knowledge required to operate said sensors.

[Reddit community](https://www.reddit.com/r/SparkKit/)

Running on Linux
-----------------
Make sure you have:
* Python 3.5
* Python module: PyGObject
* Package manager: gobject-introspection, python3-dev, libgirepository1.0-dev, libgtk-3-dev

