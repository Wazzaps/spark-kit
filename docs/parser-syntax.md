Parser syntax for Spark Kit documents:
======================================

##### Comments

    # Hello world

##### Read n bytes into buffer

    read <n> bytes

##### Load n bits from buffer into variable

    <var name>:<n>

##### Same but into an array (n*i bits)

    <var name>:<n>[<i>]

##### If statement

    <expression> ?
      <statements>
    else:
      <statements>

##### You can also combine a load and if

    <var name>:<n> ?
      <statements>
    else:
      <statements>

##### Named block

    # Namespaces everything inside
    <block name>:
      <statements>

##### Repeated block

    # Creates an array of the inside values
    <block name>[<expression>]:
      <statements>

##### Switch

    <expression> ??
      <block name for case 0>:
        <statements>
      <block name for case 1>:
        <statements>
      <block name for case 2>:
        <statements>
      # ...
      
##### You can also replace blocks with single statements

    <expression> ??
      <statement for case 0>
      <statement for case 1>
      <statement for case 2>
      # ...
      
##### Procedures

    @<proc name>:
      <statements>
    # or
    @<proc name>(<arg1>, <arg2>, <...>):
      <statements>
      
##### Calling procedures

    @<proc name>
    # or
    @<proc name>(a, b, 1, 2)
      
##### Calling external procedures

    @@<proc name>
    # or
    @@<proc name>(a, b, 1, 2)

Example:
---------

    @main:
      read 2 bytes
      some length:4
      my container:
        alice:4
        bob:8
      read (some length) bytes
      my array[some length]:
        a:4
        b:4
      @@do something(my array, my container)
    