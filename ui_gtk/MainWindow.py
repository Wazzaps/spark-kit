#!/usr/bin/python
# -*- coding: utf8 -*-
from gi.repository import Gtk, Gdk, GdkPixbuf, GObject
from skit_lib.Controllers.Misc import log

import skit_lib.Controllers.PackageLoader as PackLoader


class MainWindow(Gtk.ApplicationWindow):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Setup window and state
        self.set_default_size(520, 400)
        self.connect("delete_event", self.delete_event)
        self.main_container = Gtk.VBox()
        self.add(self.main_container)
        self.exp_controller = None
        self.mw_controller = None

        # Experiment time
        self.time_display = Gtk.Button(label="btn")
        self.time_display.set_tooltip_text("Start/Stop experiment")

        # Storage indicator
        self.storage_indicator_dc_icon = load_icon("common/res/sdcard_disconnected.svg", 16)
        self.storage_indicator_fau_icon = load_icon("common/res/sdcard_faulty.svg", 16)
        self.storage_indicator_con_icon = load_icon("common/res/sdcard_connected.svg", 16)
        self.storage_indicator = Gtk.Button(relief=Gtk.ReliefStyle.NONE)
        self.storage_indicator.set_size_request(14, 14)

        # Controller indicator
        self.cpu_indicator_dc_icon = load_icon("common/res/cpu_disconnected.svg", 16)
        self.cpu_indicator_con_icon = load_icon("common/res/cpu_connected.svg", 16)
        self.controller_indicator = Gtk.Button(relief=Gtk.ReliefStyle.NONE)
        self.controller_indicator.set_size_request(14, 14)

        # Header bar
        self.header_bar = Gtk.HeaderBar()
        self.header_bar.set_show_close_button(True)
        self.header_bar.props.custom_title = self.time_display
        self.header_bar.show_all()
        self.set_titlebar(self.header_bar)

        # Contents
        # self.perhipherals = Gtk.ListStore(str, str, str)
        # self.perhipherals.append(["spark-kit-usb-symbolic", "Mouse", "x:3\ty:7"])
        # self.perhipherals.append(["spark-kit-bluetooth-symbolic", "Distance sensor", "123 cm"])
        # self.perhipherals.append(["spark-kit-ethernet-symbolic", "Temperature sensor", "25 °C"])
        # self.perhipherals.append(["spark-kit-ethernet-symbolic", "Light sensor", "74 lux"])

        # Create the list
        self.scroll_view = Gtk.ScrolledWindow()
        self.scroll_view.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
        self.list = Gtk.TreeView()
        self.list.set_headers_visible(False)

        # Create each of the columns
        column = Gtk.TreeViewColumn("")
        renderer1 = Gtk.CellRendererPixbuf()
        renderer1.props.width = 35
        renderer2 = Gtk.CellRendererText(markup=True)
        renderer2.props.size = 1024 * 12
        renderer3 = Gtk.CellRendererText(markup=True)
        renderer3.props.size = 1024 * 12
        renderer3.props.xalign = 1.0

        # Pack them and render them properly
        column.pack_start(renderer1, False)
        column.pack_start(renderer2, True)
        column.pack_start(renderer3, False)
        column.add_attribute(renderer1, "icon-name", 0)
        column.add_attribute(renderer2, "markup", 1)
        column.add_attribute(renderer3, "markup", 2)
        self.list.append_column(column)
        self.list.connect("row-activated", self.row_clicked)
        self.list.connect("button-press-event", self.list_clicked)
        self.scroll_view.add(self.list)
        self.main_container.pack_start(self.scroll_view, True, True, 0)

        # Right click menu
        self.popover = Gtk.Popover()

        css_provider = Gtk.CssProvider()
        css_provider.load_from_data(b".popover_vbox .label {padding: 3px 11px;}")
        Gtk.StyleContext.add_provider_for_screen(
            Gdk.Screen.get_default(),
            css_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )

        self.popover_vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.popover_vbox.set_border_width(11)
        context = self.popover_vbox.get_style_context()
        context.add_class("popover_vbox")
        self.popover.add(self.popover_vbox)
        self.popover.set_position(Gtk.PositionType.BOTTOM)

        # Status bar
        self.statusbar = Gtk.HBox()
        self.statusbar_message = Gtk.Label(label="0 Devices Connected", halign=1)
        self.statusbar.pack_start(self.statusbar_message, True, True, 10)
        self.statusbar.pack_start(self.storage_indicator, False, False, 0)
        self.statusbar.pack_start(self.controller_indicator, False, False, 0)
        self.main_container.pack_start(self.statusbar, False, False, 0)

        # Icon
        self.set_icon_name("spark-kit")

        # Shortcuts / Accelerators
        self.connect("key-press-event", self._key_press_event)

        self.show_all()

    def delete_event(self, _event, _data=None):
        log('---- Shutting down ----')
        for view in self.exp_controller.open_views:
            view.hide()
        self.hide()
        GObject.timeout_add(50, self.really_exit)
        return True

    def really_exit(self):
        PackLoader.de_init()
        for view in self.exp_controller.open_views:
            view.destroy()
        self.destroy()

    def _key_press_event(self, _widget, event):
        keyval = event.keyval
        keyval_name = Gdk.keyval_name(keyval)
        # state = event.state
        # ctrl = (state & Gdk.CONTROL_MASK)
        if keyval_name == 'r':
            self.exp_controller.device_manager.reset_integrals()
        return True

    def row_clicked(self, _widget, row, _col):
        self.exp_controller.open_default_view(row)

    def list_clicked(self, widget, event):
        # Only right click
        if event.button == 3:
            # Which row was clicked?
            try:
                row, _, _, _ = widget.get_path_at_pos(int(event.x), int(event.y))
            except TypeError:
                pass
            else:
                rect = Gdk.Rectangle()
                rect.x, rect.y, rect.width, rect.height = event.x, event.y, 1, 1
                self.popover.set_relative_to(widget)
                self.popover.set_pointing_to(rect)

                for button in self.popover_vbox.get_children():
                    self.popover_vbox.remove(button)

                buttons = self.mw_controller.render_popover(row)

                for button_name, button_callback in buttons.items():
                    item = Gtk.ModelButton(button_name)
                    item.props.xalign = 0.0
                    item.connect('clicked', button_callback)
                    self.popover_vbox.pack_start(item, False, True, 0)

                self.popover.show_all()


def load_pixbuf(path, size):
    return GdkPixbuf.Pixbuf.new_from_file_at_scale(path, size, size, True)


def load_icon(path, size):
    icon = load_pixbuf(path, size)
    image = Gtk.Image.new_from_pixbuf(icon)
    return image
