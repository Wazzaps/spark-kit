#!/usr/bin/python3
import sys
import os
sys.path.append(os.getcwd())
import gi
gi.require_version('Gtk', '3.0')

from ui_gtk.MainWindowController import MainWindowController
from ui_gtk.Plot import PlotWindow

from gi.repository import Gtk, Gio, GLib
from ui_gtk.MainWindow import MainWindow
from skit_lib.Models.ProgramState import ProgramState
from skit_lib.Controllers.ExperimentController import ExperimentController
import skit_lib.Controllers.PackageLoader as PackLoader
import skit_lib.Controllers.Misc as misc


class Application(Gtk.Application):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, application_id="it.sparkk",
                         flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE,
                         **kwargs)
        self.state = None
        self.exp_controller = None
        self.window = None
        self.window_controller = None

        # Command line options
        self.add_main_option("test", ord("t"), GLib.OptionFlags.NONE,
                             GLib.OptionArg.NONE, "Command line test", None)

    def do_startup(self):
        misc.log('Spark Kit v{}.{:02} - GTK frontend'.format(*misc.VERSION))
        Gtk.Application.do_startup(self)
        PackLoader.init()

        # action = Gio.SimpleAction.new("about", None)
        # action.connect("activate", self.on_about)
        # self.add_action(action)

        action = Gio.SimpleAction.new("quit", None)
        action.connect("activate", self.on_quit)
        self.add_action(action)

        # Menu and Custom icons
        builder = Gtk.Builder()
        builder.add_from_file("ui_gtk/menu.xml")
        self.set_app_menu(builder.get_object("app-menu"))

    def do_activate(self):
        # We only allow a single window and raise any existing ones
        if not self.window:
            # Windows are associated with the application
            # when the last one is closed the application shuts down
            self.state = ProgramState()

            misc.log('---- Engine loading complete ----')

            self.window = MainWindow(application=self, title="Spark Kit")
            self.window_controller = MainWindowController(main_window=self.window)
            self.exp_controller = ExperimentController(self.state, self.window_controller)
            self.window_controller.exp_controller_loaded(self.exp_controller)
            self.window.mw_controller = self.window_controller
            self.window.exp_controller = self.exp_controller

        self.window.present()

    def do_command_line(self, command_line):
        options = command_line.get_options_dict()

        # if options.contains("test"):
        #     # This is printed on the main instance
        #     print("Test argument recieved")

        self.activate()
        return 0

    """
    def on_about(self, action, param):
        about_dialog = Gtk.AboutDialog(transient_for=self.window, modal=True)
        about_dialog.present()
    """

    def on_quit(self, _action, _param):
        self.quit()


if __name__ == "__main__":
    app = Application()
    app.run(sys.argv)
