from gi.repository import Gtk, GObject
from collections import OrderedDict
from skit_lib.Models.ProgramState import ProgramState


class MainWindowController:
    def __init__(self, main_window):
        self.mw = main_window
        self.peripheral_listview = Gtk.ListStore(str, str, str)  # icon, name, pretty value
        main_window.list.props.model = self.peripheral_listview
        self.draw_time(0)
        self.draw_indicators(ProgramState.StorageState.DISCONNECTED, ProgramState.ControllerState.DISCONNECTED)
        self.exp_controller = None

    def exp_controller_loaded(self, exp_controller):
        self.exp_controller = exp_controller
        self.mw.time_display.connect("clicked", exp_controller.toggle_timer)
        GObject.timeout_add(10, self.exp_controller.draw_current_time)
        GObject.timeout_add(50, self.exp_controller.render_and_update)

    def slower_rendering(self):
        GObject.timeout_add(50, self.exp_controller.render_and_update)

    def faster_rendering(self):
        GObject.idle_add(self.exp_controller.render_and_update)

    def clear_list(self):
        self.peripheral_listview.clear()

    def add_device(self, icon, title, value):
        return self.peripheral_listview.append((icon, title, value))

    def update_device(self, iterator, value):
        self.peripheral_listview.set(iterator, 2, value)

    def set_status_bar_text(self, text):
        self.mw.statusbar_message.set_text(text)

    def draw_time(self, experiment_time):
        self.mw.time_display.set_label(("<span font_size=\"x-large\">{}:{:02d}:{:02d}</span>"
                                        "<span font_size=\"small\" rise=\"4096\">{:03d}</span>").format(
            int(experiment_time / 1000 / 60 / 60),  # Hours
            int(experiment_time / 1000 / 60) % 60,  # Minutes
            int(experiment_time / 1000) % 60,       # Seconds
            experiment_time % 1000                  # Milliseconds
        ))
        for child in self.mw.time_display.get_children():
            child.set_use_markup(True)

    def open_default_view_factory(self, row):
        def open_default_view(_widget):
            self.exp_controller.open_default_view(row)
        return open_default_view

    def dummy_func(self, *_args, **_kwargs):
        print('Dummy')

    def render_popover(self, row):
        return OrderedDict([
            ('Open {}'.format('simple plot'), self.open_default_view_factory(row)),
            ('Switch proxy', self.dummy_func),
            ('Rename', self.dummy_func)
        ])

    def draw_indicators(self, storage_state, controller_state):
        if storage_state == ProgramState.StorageState.CONNECTED:
            self.mw.storage_indicator.set_image(self.mw.storage_indicator_con_icon)
            self.mw.storage_indicator.set_tooltip_text("SD Card module is connected and working.")
        elif storage_state == ProgramState.StorageState.DISCONNECTED:
            self.mw.storage_indicator.set_image(self.mw.storage_indicator_dc_icon)
            self.mw.storage_indicator.set_tooltip_text("SD Card module isn't connected, using PC storage.")
        elif storage_state == ProgramState.StorageState.FAULTY:
            self.mw.storage_indicator.set_image(self.mw.storage_indicator_fau_icon)
            self.mw.storage_indicator.set_tooltip_text("SD Card module is faulty, replace it.")
        elif storage_state == ProgramState.StorageState.FULL:
            self.mw.storage_indicator.set_image(self.mw.storage_indicator_fau_icon)
            self.mw.storage_indicator.set_tooltip_text("SD Card module is full, replace it or empty contents.")

        if controller_state == ProgramState.ControllerState.CONNECTED:
            self.mw.controller_indicator.set_image(self.mw.cpu_indicator_con_icon)
            self.mw.controller_indicator.set_tooltip_text("Controller module is connected.")
        elif controller_state == ProgramState.ControllerState.DISCONNECTED:
            self.mw.controller_indicator.set_image(self.mw.cpu_indicator_dc_icon)
            self.mw.controller_indicator.set_tooltip_text("Controller module isn't connected, using PC.")
