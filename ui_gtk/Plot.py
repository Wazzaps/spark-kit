#!/usr/bin/python
# -*- coding: utf8 -*-
import random
import math
import cairo

from gi.repository import Gtk, Gdk, GObject


class PlotWindow(Gtk.Window):
    def __init__(self, exp_data, identifier, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.exp_data = exp_data
        self.identifier = identifier

        # Setup window and state
        self.is_closed = False
        self.set_default_size(300, 300)
        self.connect("delete_event", self.delete_event)
        self.size = (300, 300)

        self.canvas = Gtk.DrawingArea()
        self.canvas.connect('draw', self.draw_handler)
        self.canvas.connect('size-allocate', self.resize_handler)
        # canvas.set_size_request(250, 250)
        self.add(self.canvas)

        # Icon
        self.set_icon_name("spark-kit")

        # Shortcuts / Accelerators
        self.connect("key-press-event", self._key_press_event)

        self.show_all()
        GObject.timeout_add(10, self.trigger_draw)

    def delete_event(self, _event, _data=None):
        self.is_closed = True
        self.destroy()
        return True

    def _key_press_event(self, _widget, event):
        keyval = event.keyval
        keyval_name = Gdk.keyval_name(keyval)
        # state = event.state
        # ctrl = (state & Gdk.CONTROL_MASK)
        # print('Plot', keyval_name)
        return True

    def draw_point(self, cr, x, y):
        cr.arc(x * self.size[0],
               y * self.size[1], 2.5, 0, 2 * math.pi)
        cr.fill()

    def draw_handler(self, _widget, cr):
        cr.set_source_rgb(.9, .9, .9)
        cr.rectangle(0, 0, self.size[0], self.size[1])
        cr.fill()

        data = list(self.exp_data.last_x_seconds(self.identifier, self.size[0] / 150))
        if len(data) > 1:
            oldest_time = data[-1][0] - self.size[0] / 150
            latest_time = data[-1][0]

            # Draw lines
            cr.set_source_rgb(1, .5, .2)
            cr.set_line_width(1.5)
            cr.new_path()
            cr.move_to(0, (1 - (data[0][2] / 5)) * self.size[1])
            for sample in data:
                cr.line_to((sample[0] - oldest_time) / (latest_time - oldest_time) * self.size[0],
                           (1 - (sample[2] / 5)) * self.size[1])
            cr.stroke()

            # Draw points
            cr.set_source_rgb(.7, .2, .2)
            for sample in data:
                self.draw_point(cr, (sample[0] - oldest_time) / (latest_time - oldest_time), (1 - (sample[2] / 5)))

    def resize_handler(self, _widget, rect):
        self.size = (rect.width, rect.height)
        # print
        # self.canvas.set_size_request(rect.width, rect.height)

    def trigger_draw(self):
        self.canvas.queue_draw()
        return True
