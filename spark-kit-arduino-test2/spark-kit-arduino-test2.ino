// Simulates some simple devices:
// 1. Analog reader

// Todo:
// SD card
// RTC

void setup() {
  // bitSet(UCSR0A, U2X0);
  Serial.begin(115200);
  pinMode(11, INPUT_PULLUP);
  pinMode(12, INPUT_PULLUP);
}

void loop() {
  while(!Serial.available());
  uint8_t cmd = Serial.read();
  switch(cmd >> 6) {
  case 0: // Identify
    // 1 device
    Serial.write(1);
    send_signature();
    break;
  case 1: // Trigger

    break;
  case 2: // Get
    switch(cmd & 0x3F) {
    case 0: // Fresh sample
    case 1: // Saved sample
    case 2: // Stale sample
    case 3: // Average sample
    case 4: // Min sample
    case 5: // Max sample
      uint16_t sample1 = analogRead(0);
      Serial.write(sample1 % 256);
      Serial.write(sample1 / 256);
    }
    break;
  case 3: // Direct Message

    break;
  }
}

void send_signature() {
  uint8_t sig1[48] = {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
  for (int i = 0; i < 48; i++)
    Serial.write(sig1[i]);
}

