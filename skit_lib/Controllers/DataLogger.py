from collections import deque, OrderedDict
import time
import itertools
import csv

from skit_lib.Controllers import DeviceManager


def flatten(l):
    if type(l[0]) == tuple or type(l[0]) == list:
        return [item for sublist in l for item in sublist]
    else:
        return l


class DataLogger:
    def __init__(self, precision=3):
        self.labels = {}
        self.log_data = OrderedDict()
        self.view_data = OrderedDict()
        self.start_time = 0
        self.precision = precision

    def start(self):
        self.start_time = time.monotonic()

    def update_labels(self, identifier, log_labels, view_labels):
        self.labels[identifier] = (log_labels, view_labels)

    def add_point(self, curr_time, identifier, log_value, view_value):
        self.log_data.setdefault(identifier, deque())
        self.view_data.setdefault(identifier, deque())
        new_time = curr_time - self.start_time

        if len(self.log_data[identifier]) == 0 or round(new_time, self.precision) != self.log_data[identifier][-1][0]:
            rounded_time = (round(new_time, self.precision),)

            timed_log_value = rounded_time + (log_value,)
            self.log_data[identifier].append(timed_log_value)

            timed_view_value = rounded_time + (view_value,)
            self.view_data[identifier].append(timed_view_value)

    def clear(self):
        self.log_data = OrderedDict()
        self.view_data = OrderedDict()

    def last_x_seconds(self, identifier, secs, is_view=True):
        data = self.view_data if is_view else self.log_data
        if identifier in data:
            latest_time = data[identifier][-1][0]

            for i, sample in enumerate(reversed(data[identifier])):
                if sample[0] < latest_time - secs:
                    index = i
                    break
            else:
                index = len(data[identifier])

            return itertools.islice(data[identifier],
                                    len(data[identifier]) - index, len(data[identifier]))
        else:
            return []

    def shelf(self, secs):
        pass  # TODO

    def export_to_csv(self, fd):
        writer = csv.writer(fd)
        labels = []

        for identifier in self.log_data.keys():
            for label in flatten(DeviceManager.get_device_type(identifier).VALUE_LABELS):
                labels.append('{} ({}): {}'.format(
                    DeviceManager.get_device_name(identifier)[1],
                    identifier[2],
                    label
                ))

        writer.writerow(['Time'] + labels)

        # Group data by time
        output = {}
        for identifier, samples in self.log_data.items():
            for sample in samples:
                output.setdefault(sample[0], {})
                output[sample[0]][identifier] = sample[1:]

        # Sort dict by time
        output = OrderedDict(sorted(output.items(), key=lambda s: s[0]))

        vals = OrderedDict([(identifier, samples[0][1:]) for identifier, samples in self.log_data.items()])

        for sample_time, devices in output.items():
            for identifier, values in devices.items():
                vals[identifier] = values
            row = [sample_time]
            for identifier in self.log_data.keys():
                row += flatten(vals[identifier])
            writer.writerow(row)
