import time

from skit_lib.Controllers.DeviceManager import DeviceManager
import threading
import skit_lib.Controllers.PackageLoader as PackLoader


class ExperimentController:
    def __init__(self, state, mw_controller):
        self.state = state
        mw_controller.exp_controller = self
        self.mw_controller = mw_controller
        self.lock = threading.Lock()
        self.device_manager = DeviceManager(PackLoader.modules['Interfaces'], self.lock, state.exp_data)
        self.ui_iterators = {}
        self.was_in_fast_mode = False
        self.open_views = []
        self.i = 0
        self.x = 0
        self.y = 0
        mw_controller.slower_rendering()

    @staticmethod
    def format_device_name(name, identifier):
        return '<span font_size=\"x-small\">{0} - {2:x}</span>\n{1}'.format(name[0], name[1], identifier)

    def render_and_update(self):
        with self.lock:
            if self.device_manager.should_render:
                self.ui_iterators.clear()
                self.mw_controller.clear_list()
                for interface_name, interface in self.device_manager.devs_by_iface.items():
                    module = PackLoader.modules['Interfaces'][interface_name]['module'].Module
                    for identifier, values in interface.items():
                        self.ui_iterators[identifier] = self.mw_controller.add_device(
                            module.interface_icon,
                            self.format_device_name(
                                PackLoader.mapping_table[identifier[:2]][0],
                                identifier[2]
                            ),
                            values[3]
                        )
                self.mw_controller.set_status_bar_text(
                    '{} Devices Connected'.format(len(self.mw_controller.peripheral_listview))
                )
            if self.device_manager.should_update or self.device_manager.should_render:
                for interface_name, interface in self.device_manager.devs_by_iface.items():
                    for identifier, values in interface.items():
                        self.mw_controller.update_device(self.ui_iterators[identifier[:3]], values[3])

            self.device_manager.should_update = False
            self.device_manager.should_render = False

        if self.was_in_fast_mode and not self.device_manager.fast_mode:
            self.mw_controller.slower_rendering()
            self.was_in_fast_mode = False
        elif not self.was_in_fast_mode and self.device_manager.fast_mode:
            self.mw_controller.faster_rendering()
            self.was_in_fast_mode = True
        else:
            return not PackLoader.exiting
        return False

    def draw_current_time(self):
        if self.state.is_experiment_running:
            self.state.experiment_time = int((time.time() - self.state.experiment_start_time) * 1000)
            self.mw_controller.draw_time(self.state.experiment_time)
        return True  # Makes sure the timer keeps ticking

    def toggle_timer(self, _):
        self.state.is_experiment_running = not self.state.is_experiment_running
        if self.state.is_experiment_running:
            # Reset time
            self.state.experiment_start_time = time.time()

            self.device_manager.enable_fast_mode()
            self.state.exp_data.clear()
            self.state.exp_data.start()
            self.device_manager.is_recording = True
        else:
            # Update one last time
            self.state.experiment_time = int((time.time() - self.state.experiment_start_time) * 1000)
            self.mw_controller.draw_time(self.state.experiment_time)

            self.device_manager.disable_fast_mode()
            self.device_manager.is_recording = False
            with open('/tmp/sparkkit_out.csv', 'w') as fd:
                self.state.exp_data.export_to_csv(fd)

    def open_default_view(self, row):
        devices = [dev for _, iface in self.device_manager.devs_by_iface.items() for dev in iface]
        identifier = devices[row.get_indices()[0]]
        name, module = PackLoader.mapping_table[identifier[:2]]
        pretty_name = '{} ({:x})'.format(name[1], identifier[2])
        the_view = PackLoader.modules['Views'][module.Module.DEFAULT_VIEW]['module'].Module
        self.open_views.append(the_view(self.state.exp_data, identifier, title=pretty_name))
