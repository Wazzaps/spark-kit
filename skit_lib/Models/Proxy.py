# noinspection PyMethodMayBeStatic
class Proxy:
    INPUT_TYPE = (float, str, float, str, float, str)
    OUTPUT_TYPE = (float, str, float, str, float, str)

    def reset(self):
        pass

    def next(self, val):
        return val

    def mutate_labels(self, labels):
        return labels


class AnyType:
    pass
