import json
from enum import Enum

from skit_lib.Controllers.DataLogger import DataLogger
from skit_lib.Controllers.Misc import log


class ProgramState:
    class StorageState(Enum):
        FAULTY = 0
        DISCONNECTED = 1
        CONNECTED = 2
        FULL = 3

    class ControllerState(Enum):
        DISCONNECTED = 0
        CONNECTED = 1

    def __init__(self):
        # Runtime
        self.is_experiment_running = 0
        self.storage_state = self.StorageState.DISCONNECTED
        self.controller_state = self.ControllerState.DISCONNECTED

        # Save-able
        self.exp_time = 0  # in ms
        self.exp_data = DataLogger(precision=9)
        self.exp_proxies = {}

        # Config
        with open('config.json', 'r') as fd:
            self.config = json.load(fd)
        log('Configuration file loaded!')
