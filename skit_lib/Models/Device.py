import time


class Device:
    VALUE_LABELS = ('Derivative', 'Value', 'Integral')
    DEFAULT_PROXIES = ['log', 'Base.Proxies_All.ExtractValue', 'view']
    DEFAULT_VIEW = 'Base.Views_GTK_All.SimplePlot'
    DEFAULT_MINIVIEW = 'Base.MiniViews_GTK_All.SimpleValue'
    OUTPUT_TYPE = (float, str, float, str, float, str)

    def __init__(self, identifier):
        self.set_value_handlers = []
        self.identifier = identifier

        self.last_value = 0.0
        self.last_value_time = 0.0

        self.pretty_derivative = 'None'
        self.derivative = 0.0

        self.pretty_value = 'None'
        self.value = 0.0

        self.pretty_integral = 'None'
        self.integral = 0.0

    def pretty_value_formatter(self):
        self.pretty_derivative = '{:.02} units'.format(self.derivative)
        self.pretty_value = '{:.02} units'.format(self.value)
        self.pretty_integral = '{:.02} units'.format(self.integral)

    def set_value(self, value):
        self.last_value = self.value
        if self.last_value_time != 0:
            self.derivative = (value - self.last_value) / (time.monotonic() - self.last_value_time)
            self.integral += value * (time.monotonic() - self.last_value_time)
        self.value = value

        self.last_value_time = time.monotonic()

        self.pretty_value_formatter()

        for handler in self.set_value_handlers:
            handler(self)

    def reset_integral(self):
        self.integral = 0

    def simplify(self):
        return self.identifier,\
               self.derivative, self.pretty_derivative,\
               self.value, self.pretty_value,\
               self.integral, self.pretty_integral

