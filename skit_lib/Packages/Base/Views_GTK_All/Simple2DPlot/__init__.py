#!/usr/bin/python
# -*- coding: utf8 -*-
import math

from gi.repository import Gtk, Gdk, GObject


def hsl_to_rgb(h, s, l):
    c = (1.0 - abs(2.0 * l - 1)) * s
    h /= 60.0
    x = c * (1.0 - abs(h % 2.0 - 1.0))
    if 0 <= h <= 1:
        return c, x, 0
    elif 1 <= h <= 2:
        return x, c, 0
    elif 2 <= h <= 3:
        return 0, c, x
    elif 3 <= h <= 4:
        return 0, x, c
    elif 4 <= h <= 5:
        return x, 0, c
    elif 5 <= h <= 6:
        return c, 0, x
    else:
        return 0, 0, 0


class Module(Gtk.Window):
    INPUT_TYPE = (float, float)

    def __init__(self, data, identifier, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.data = data
        self.identifier = identifier

        # Setup window and state
        self.is_closed = False
        self.set_default_size(300, 300)
        self.connect("delete_event", self.delete_event)
        self.size = (300, 300)

        self.canvas = Gtk.DrawingArea()
        self.canvas.connect('draw', self.draw_handler)
        self.canvas.connect('size-allocate', self.resize_handler)
        # canvas.set_size_request(250, 250)
        self.add(self.canvas)

        # Icon
        self.set_icon_name("spark-kit")

        # Shortcuts / Accelerators
        # self.connect("key-press-event", self._key_press_event)

        self.show_all()
        GObject.timeout_add(10, self.trigger_draw)

    def delete_event(self, _event, _data=None):
        self.is_closed = True
        self.destroy()
        return True

    # def _key_press_event(self, _widget, event):
    #     keyval = event.keyval
    #     keyval_name = Gdk.keyval_name(keyval)
    #     # state = event.state
    #     # ctrl = (state & Gdk.CONTROL_MASK)
    #     print('Plot', keyval_name)
    #     return True

    def draw_point(self, cr, x, y, radius):
        cr.arc(x * self.size[0],
               y * self.size[1], radius, 0, 2 * math.pi)
        cr.fill()

    def draw_handler(self, _widget, cr):
        cr.set_source_rgb(.9, .9, .9)
        cr.rectangle(0, 0, self.size[0], self.size[1])
        cr.fill()

        data = list(self.data.last_x_seconds(self.identifier, self.size[0] / 150))
        if len(data) > 1:
            oldest_time = data[-1][0] - self.size[0] / 150
            latest_time = data[-1][0]

            # Draw axes
            cr.set_source_rgb(1, .5, .2)
            cr.set_line_width(1.5)
            cr.line_to(0.5 * self.size[0], 0)
            cr.line_to(0.5 * self.size[0], self.size[1])
            cr.stroke()
            cr.line_to(0, 0.5 * self.size[1])
            cr.line_to(self.size[0], 0.5 * self.size[1])
            cr.stroke()

            # Draw lines
            cr.move_to((data[0][1][0] + 127) / 254 * self.size[0], (1 - (data[0][1][1] + 127) / 254) * self.size[1])
            cr.set_line_width(1.5)
            for sample in data:
                time_factor = 1 - (sample[0] - oldest_time) / (latest_time - oldest_time)
                cr.set_source_rgb(*hsl_to_rgb(time_factor * 30 + 20, 1, 0.5))
                cr.line_to((sample[1][0] + 127) / 254 * self.size[0], (1 - (sample[1][1] + 127) / 254) * self.size[1])
                cr.stroke()
                cr.move_to((sample[1][0] + 127) / 254 * self.size[0], (1 - (sample[1][1] + 127) / 254) * self.size[1])

            # Draw points
            for i, sample in enumerate(data):
                time_factor = 1 - (sample[0] - oldest_time) / (latest_time - oldest_time)
                if i == len(data) - 1:
                    cr.set_source_rgb(1, 0, 0)
                else:
                    cr.set_source_rgb(*hsl_to_rgb(time_factor * 30 + 20, 1.0, 0.5))
                self.draw_point(cr,
                                (sample[1][0] + 127) / 254,
                                1 - (sample[1][1] + 127) / 254,
                                4 if i == len(data) - 1 else 2 + time_factor)

    def resize_handler(self, _widget, rect):
        self.size = (rect.width, rect.height)

    def trigger_draw(self):
        self.canvas.queue_draw()
        return True
