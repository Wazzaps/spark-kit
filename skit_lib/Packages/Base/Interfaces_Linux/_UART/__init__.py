import glob
import threading
import serial
import time
import select
import skit_lib.Models.Interface
import skit_lib.Controllers.DeviceManager as DeviceManager
from skit_lib.Controllers.Misc import log


def sparkkit_get(ser, devices, get_type):
    if type(get_type) == str:
        return sparkkit_get(ser, devices, ['fresh', 'saved', 'stale', 'avg', 'min', 'max'].index(str(get_type)))
    elif type(get_type) == int:
        assert 0 <= get_type <= 63
        ser.write(bytearray([128 + get_type]))

        amounts = []
        amount_sum = 0
        data = bytes()
        results = {}
        for (index, (identifier, device)) in enumerate(devices.items()):
            amount = device.uart_parse_amount()
            if amount != -1:
                amounts.append(amount)
                amount_sum += amount
            else:
                amounts.append(0)
                if amount_sum > 0:
                    data += ser.read(amount_sum)
                    amount_sum = 0
                results[identifier] = device.uart_custom_parse(ser)
        if amount_sum > 0:
            data += ser.read(amount_sum)
            amount_sum = 0
            for (index, (identifier, device)) in enumerate(devices.items()):
                if amounts[index] > 0:
                    results[identifier] = device.uart_post_parse(data[amount_sum:amount_sum + amounts[index]])
                    amount_sum += amounts[index]
        return results

    else:
        raise TypeError("Supply either a get_type id or a get_type name")


class Module(skit_lib.Models.Interface.Interface):
    interface_pretty_name = 'UART Serial'
    interface_icon = 'spark-kit-ethernet-symbolic'

    def __init__(self, *args):
        self.lock = threading.Lock()
        self.ser_container = [None]
        self.ser = None

        super().__init__(*args, lock=self.lock, ser_container=self.ser_container)

    def get_interface(self):
        available_ports = glob.glob('/dev/ttyUSB*')
        if not available_ports:
            return False

        self.ser = serial.Serial(available_ports[0], 115200)

        writable = select.select([], [self.ser], [], 3)[1]
        if not self.ser or not self.ser.is_open or self.ser not in writable:
            return False

        log('UART "{}" Connected'.format(available_ports[0]))
        # Wait for Arduino to boot up
        time.sleep(2.5)
        self.ser.reset_input_buffer()
        log('UART "{}" Ready'.format(available_ports[0]))

        # Give access to seeker
        # noinspection PyTypeChecker
        self.ser_container[0] = self.ser
        return True

    def get_values(self):
        with self.lock:
            results = sparkkit_get(self.ser, self.devices, 'fresh')

            for identifier, result in results.items():
                self.devices[identifier].set_value(result)

            simplified_devices = [dev.simplify() for _, dev in self.devices.items()]

            self.value_queue.put(simplified_devices)
        return True


class SeekThread(skit_lib.Models.Interface.SeekThread):
    def __init__(self, *args, lock, ser_container):
        super().__init__(*args)
        self.lock = lock
        self.ser_container = ser_container
        self.ser = self.ser_container[0]

    def get_devices(self):
        self.ser = self.ser_container[0]
        try:
            if self.ser is None or not self.ser.is_open:
                self.seek_queue.put([])
                return

            output = []
            seek_output = []

            with self.lock:
                self.ser.write(b'\0')  # 'Identify'
                device_count = int(self.ser.read(1)[0])  # TODO: Add varint handling here

                for dev_id in range(device_count):
                    mfr_id = int.from_bytes(self.ser.read(16), byteorder='little')
                    prod_id = int.from_bytes(self.ser.read(16), byteorder='little')
                    ser_num = int.from_bytes(self.ser.read(16), byteorder='little')
                    output.append((mfr_id, prod_id, ser_num, dev_id))
                    seek_output.append((mfr_id, prod_id, ser_num))

                if list(sorted(self.devices)) != sorted(seek_output):
                    # print([device.identifier for device in self.devices], 'To', output)
                    self.devices.clear()
                    for device in output:
                        dev = DeviceManager.create_device(device)
                        self.devices[device[:3]] = dev
                    self.seek_queue.put(seek_output)

        except serial.serialutil.SerialException:
            self.seek_queue.put([])

    def uninit(self):
        if self.ser and self.ser.is_open:
            self.ser.close()
