import socket
import skit_lib.Packages.Base.Interfaces_Linux._UART
from skit_lib.Controllers.Misc import log


class FakeSerial:
    PORT = 1

    def __init__(self, addr):
        self.is_open = True

        self.sock = socket.socket(socket.AF_BLUETOOTH, socket.SOCK_STREAM, socket.BTPROTO_RFCOMM)
        self.sock.connect((addr, FakeSerial.PORT))

        self.sock.settimeout(0.1)
        log('Bluetooth UART "{}" Connected'.format(addr))
        while 1:
            try:
                self.sock.recv(1)
            except socket.timeout:
                break
        log('Bluetooth UART "{}" Ready'.format(addr))

        self.sock.settimeout(None)

    def read(self, count):
        output = bytes()
        for _ in range(count):
            try:
                output += self.sock.recv(1)
            except socket.timeout:
                break
        return output

    def write(self, data):
        self.sock.send(bytes(data))

    def close(self):
        self.is_open = False
        self.sock.close()


class Module(skit_lib.Packages.Base.Interfaces_Linux._UART.Module):
    interface_pretty_name = 'UART Serial over Bluetooth'
    interface_icon = 'spark-kit-bluetooth-symbolic'

    def get_interface(self):
        self.ser = FakeSerial('00:21:13:04:73:63')
        # available_ports = glob.glob('/dev/rfcomm*')
        # if not available_ports:
        #     return False
        #
        # self.ser = serial.Serial(available_ports[0], 115200)
        #
        # writable = select.select([], [self.ser], [], 3)[1]
        # if not self.ser or not self.ser.is_open or self.ser not in writable:
        #     return False
        #
        # log('UART "{}" Connected'.format(available_ports[0]))
        # # Wait for Arduino to boot up
        # time.sleep(2.5)
        # self.ser.reset_input_buffer()
        # log('UART "{}" Ready'.format(available_ports[0]))

        # Give access to seeker
        # noinspection PyTypeChecker
        self.ser_container[0] = self.ser
        return True


class SeekThread(skit_lib.Packages.Base.Interfaces_Linux._UART.SeekThread):
    pass
