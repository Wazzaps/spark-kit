import skit_lib.Models.Device

device_types = {
    (0, 1): ('Example devices', 'Sine wave')
}


class Module(skit_lib.Models.Device.Device):
    DEFAULT_PROXIES = ['log', 'Base.Proxies_All.ExtractValue', 'Base.Proxies_All.AdjustSineWave', 'view']
