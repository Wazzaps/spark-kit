import skit_lib.Packages.Base.Devices_All.UARTSingleValueDeviceBase

device_types = {
    (1, 1): ('Testing', 'Simple analog reader')
}


class Module(skit_lib.Packages.Base.Devices_All.UARTSingleValueDeviceBase.Module):
    VALUE_LABELS = ('dv', 'volts', 'integral')

    def set_value(self, value):
        super().set_value(value / 1023.0 * 5)

    def pretty_value_formatter(self):
        self.pretty_derivative = '{} units'.format(self.derivative)
        self.pretty_value = '{0:.3f} volts'.format(self.value)
        self.pretty_integral = '{} units'.format(self.integral)
