import skit_lib.Models.Proxy


class Module(skit_lib.Models.Proxy.Proxy):
    INPUT_TYPE = ((float, float), str, (float, float, int, int, int), str, (float, float))
    OUTPUT_TYPE = (float, float)

    def next(self, val):
        return val[2]

    def mutate_labels(self, labels):
        return labels[1]
