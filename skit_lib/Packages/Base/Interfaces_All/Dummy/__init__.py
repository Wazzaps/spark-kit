import math
import time

import skit_lib.Models.Interface
from skit_lib.Controllers import DeviceManager


class Module(skit_lib.Models.Interface.Interface):
    interface_pretty_name = 'Dummy device'
    interface_icon = 'application-menu-symbolic'

    def get_values(self):
        self.devices[(0, 1, 1)].set_value(math.sin(time.time()))
        self.value_queue.put([self.devices[(0, 1, 1)].simplify()])
        time.sleep(0.01)
        return True


class SeekThread(skit_lib.Models.Interface.SeekThread):
    def get_devices(self):
        if len(self.devices) == 0:
            self.devices[(0, 1, 1)] = DeviceManager.create_device((0, 1, 1))
            self.seek_queue.put([(0, 1, 1)])
